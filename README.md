# Django Template

A simple cookie cutter template to create django projects

## Variables

| Variable          |                           Description                           | Values                                                      | Default      |
|-------------------|:---------------------------------------------------------------:|-------------------------------------------------------------|--------------|
| package_name      | The name of the project                                         | str                                                         |              |
| description       | The description of the project                                  | str                                                         |              |
| version           | The default version of the project                              | str                                                         | 0.1.0        |
| year              | The year of publication                                         | int                                                         | 2021         |
| author            | The author of the project                                       | str                                                         |              |
| sphinx            | Generate a `doc` folder which hold shpinx default configuration | y / n                                                       | y            |
| coverage          | Generate a `.coveragerc` file and add coverage commands to CI   | y / n                                                       | y            |
| sentry            | Add sentry configuration to project                             | y / n                                                       | n            |
| gitlab_ci         | Generate a default `.gitlab-ci.yml` file                        | y / n                                                       | y            |
| language          | The default language of the project                             | en-US<br>fr-FR<br>de-DE<br>es-ES<br>it-IT                   | en-US        |
| timezone          | The timezone of the project                                     | Europe/Paris<br>Europe/London<br>America/New_York           | Europe/Paris |
| license           | The project's license                                           | MIT<br>BSD-3<br>GNU GPL v2.0<br>Apache Software License 2.0 | MIT          |
| production_domain | The FQDN of the project in production environnment              | str                                                         |              |
| production_path   | The path of the project in production environnment              | str                                                         |              |

## Default project tree

Here an overview of a default project created from this template :

```
example/
├── doc
│   ├── make.bat
│   ├── Makefile
│   └── source
│       ├── conf.py
│       ├── index.rst
│       └── static
├── LICENSE
├── requirements
│   ├── base.txt
│   ├── development.txt
│   ├── production.txt
│   └── test.txt
├── src
│   ├── example
│   │   ├── asgi.py
│   │   ├── __init__.py
│   │   ├── settings
│   │   │   ├── base.py
│   │   │   ├── development.py
│   │   │   ├── production.py
│   │   │   └── test.py
│   │   ├── urls.py
│   │   └── wsgi.py
│   ├── manage.py
│   └── runinenv.sh
└── utils
    └── changelog.py

9 directories, 24 files
```

## How to use ?

```sh
pip3 install cookiecutter
cookiecutter https://gitlab.com/Imotekh/django-template.git
```