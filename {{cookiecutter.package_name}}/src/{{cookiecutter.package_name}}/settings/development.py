"""
La configuration à utiliser pour les environnements de dévelopement
"""

from {{cookiecutter.package_name}}.settings.base import *

from dotenv import load_dotenv

load_dotenv(verbose=True, override=True, dotenv_path='.env')

SECRET_KEY = 'uehfj@070q!sexgtk4&5r6hho&*2&1j9vjyk#jjxe22m2=(3(y'

DEBUG = True

INSTALLED_APPS.append('debug_toolbar')
MIDDLEWARE.insert(0, 'debug_toolbar.middleware.DebugToolbarMiddleware')

ALLOWED_HOSTS = ['127.0.0.1', 'localhost']
INTERNAL_IPS = ["127.0.0.1"]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3')
    }
}

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.1/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static/')

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media/')
