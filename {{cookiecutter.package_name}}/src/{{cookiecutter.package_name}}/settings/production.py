"""
La configuration à utiliser pour les environnements de production
"""

from {{cookiecutter.package_name}}.settings.base import *

from dotenv import load_dotenv

load_dotenv(verbose=True, override=True, dotenv_path='.env')

SECRET_KEY = os.environ.get('TOKEN_PROD')
{%- if cookiecutter.sentry == 'y' %}SENTRY_DSN = os.environ.get('SENTRY_DSN'){% endif %}

DEBUG = False

ALLOWED_HOSTS = ['{{cookiecutter.production_domain}}']

DATABASES = {
    'default': {
        'ENGINE':   'django.db.backends.postgresql_psycopg2',
        'NAME':     os.environ.get('DB_NAME'),
        'USER':     os.environ.get('DB_USER'),
        'PASSWORD': os.environ.get('DB_PASSWORD'),
        'HOST':     os.environ.get('DB_HOST'),
        'PORT':     os.environ.get('DB_PORT'),
    }
}

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.1/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = '{{cookiecutter.production_path}}/static/'

MEDIA_URL = '/media/'
MEDIA_ROOT = '{{cookiecutter.production_path}}/media/'
