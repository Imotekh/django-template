"""
ASGI config for {{cookiecutter.package_name}} project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/howto/deployment/asgi/
"""

import os

from django.core.asgi import get_asgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', '{{cookiecutter.package_name}}.settings.production')

{%- if cookiecutter.sentry == 'y' %}
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration
from sentry_sdk.integrations.asgi import SentryAsgiMiddleware

from django.conf import settings

def init():
	if settings.DEBUG == False and hasattr(settings, 'SENTRY_DSN'):
		sentry_sdk.init(dsn=settings.SENTRY_DSN, integrations=[DjangoIntegration(transaction_style='url')])

def create_app(app):
	if settings.DEBUG == False and hasattr(settings, 'SENTRY_DSN'):
		return SentryAsgiMiddleware(app)
	else:
		return app

init()
application = create_app(get_asgi_application())
{%- endif %}
{%- if not cookiecutter.sentry == 'y' %}
application = get_asgi_application()
{%- endif %}
